import 'package:Motivie/myHomePage.dart';
import 'package:flutter/material.dart';
import '../mainApp.dart';
import '/models/user.dart';
import '/screens/authenticate/authenticate_screen.dart';
import 'package:provider/provider.dart';

class SplashScreenWrapper extends StatelessWidget {
  const SplashScreenWrapper({super.key});

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<AppUser?>(context);
    if (user == null) {
      return const AuthenticateScreen();
    } else {
      return const MainApp();
    }
  }
}
