import 'package:flutter/material.dart';
import '../../services/authentication.dart';
import '/common/constants.dart';
import '/common/loading.dart';
import 'Forgetpassword.dart';

class AuthenticateScreen extends StatefulWidget {
  const AuthenticateScreen({super.key});

  @override
  _AuthenticateScreenState createState() => _AuthenticateScreenState();
}

class _AuthenticateScreenState extends State<AuthenticateScreen> {
  final AuthenticationService _auth = AuthenticationService();
  final _formKey = GlobalKey<FormState>();
  String error = '';
  bool loading = false;

  final emailController = TextEditingController();
  final nameController = TextEditingController();
  final passwordController = TextEditingController();
  bool showSignIn = true;

  @override
  void dispose() {
    nameController.dispose();
    emailController.dispose();
    passwordController.dispose();
    super.dispose();
  }

  void toggleView() {
    setState(() {
      _formKey.currentState?.reset();
      error = '';
      emailController.text = '';
      nameController.text = '';
      passwordController.text = '';
      showSignIn = !showSignIn;
    });
  }

  @override
  Widget build(BuildContext context) {
    return loading
        ? Loading()
        : Scaffold(
            backgroundColor: Colors.white,
            appBar: AppBar(
              backgroundColor: Colors.blueGrey,
              elevation: 0.0,
              title: Text(showSignIn ? 'Connexion' : 'Enregistrer'),
              actions: <Widget>[
                TextButton.icon(
                  icon: const Icon(
                    Icons.person,
                    color: Colors.white,
                  ),
                  label: Text(showSignIn ? "Enregistrer" : 'Connexion',
                      style: const TextStyle(color: Colors.white)),
                  onPressed: () => toggleView(),
                ),
              ],
            ),
            body: Container(
              height: MediaQuery.of(context).size.height * 0.7,
              decoration: const BoxDecoration(
                  color: Colors.grey,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                  )),
              padding: const EdgeInsets.all(40),
              child: Form(
                key: _formKey,
                child: Column(
                  children: [
                    !showSignIn
                        ? TextFormField(
                            controller: nameController,
                            decoration:
                                textInputDecoration.copyWith(hintText: 'name'),
                            validator: (value) => value == null || value.isEmpty
                                ? "Entrer un nom"
                                : null,
                          )
                        : Container(),
                    !showSignIn ? const SizedBox(height: 15.0) : Container(),
                    TextFormField(
                      controller: emailController,
                      decoration:
                          textInputDecoration.copyWith(hintText: 'email'),
                      validator: (value) => value == null || value.isEmpty
                          ? "Entrer un email"
                          : null,
                    ),
                    const SizedBox(height: 15.0),
                    TextFormField(
                      controller: passwordController,
                      decoration:
                          textInputDecoration.copyWith(hintText: 'password'),
                      obscureText: true,
                      validator: (value) => value != null && value.length < 6
                          ? "Entrer un mot de passe avec minimum 6 characteres"
                          : null,
                    ),
                    showSignIn
                        ? TextButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                      builder: (context) =>
                                          const forgetpassword()));
                            },
                            child: const Text(
                              "Mot de passe oublié ?",
                              style: TextStyle(color: Colors.black),
                            ))
                        : Container(),
                    const SizedBox(height: 15.0),
                    ElevatedButton(
                      child: Text(
                        showSignIn ? "Connexion" : "Enregistrer",
                        style: const TextStyle(color: Colors.white),
                      ),
                      onPressed: () async {
                        if (_formKey.currentState?.validate() == true) {
                          setState(() => loading = true);
                          var password = passwordController.value.text;
                          var email = emailController.value.text;
                          var name = nameController.value.text;

                          dynamic result = showSignIn
                              ? await _auth.signInWithEmailAndPassword(
                                  email, password)
                              : await _auth.registerWithEmailAndPassword(
                                  name, email, password);
                          if (result == null) {
                            setState(() {
                              loading = false;
                              error = 'Email incorrect';
                            });
                          }
                        }
                      },
                    ),
                    const SizedBox(height: 15.0),
                    Text(
                      error,
                      style: const TextStyle(color: Colors.red, fontSize: 15.0),
                    )
                  ],
                ),
              ),
            ),
          );
  }
}
