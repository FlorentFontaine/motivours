import 'package:Motivie/screens/authenticate/authenticate_screen.dart';
import 'package:flutter/material.dart';
import 'Toast.dart';
import 'package:firebase_auth/firebase_auth.dart';

class forgetpassword extends StatefulWidget {
  const forgetpassword({super.key});

  @override
  State<forgetpassword> createState() => _forgetpasswordState();
}

class _forgetpasswordState extends State<forgetpassword> {
  final emailcon = TextEditingController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Reinitialiser votre mot de passe"),
      ),
      body: Column(children: [
        const SizedBox(
          height: 70,
        ),
        const Text(
          "Entrer votre email sur lequel vous souhaitez reinitialiser votre mot de passe",
          textAlign: TextAlign.center,
          style: TextStyle(
            fontSize: 18,
          ),
        ),
        const SizedBox(
          height: 70,
        ),
        Padding(
          padding: const EdgeInsets.all(20),
          child: TextFormField(
            controller: emailcon,
            decoration: const InputDecoration(
              filled: true,
              focusedBorder: OutlineInputBorder(
                  borderSide: BorderSide(color: Colors.black)),
              fillColor: Colors.white,
              hintText: "Entrer votre email ",
            ),
          ),
        ),
        TextButton(
            child: const Text('Envoyer'),
            onPressed: () {
              FirebaseAuth.instance
                  .sendPasswordResetEmail(email: emailcon.text.toString())
                  .then((value) {
                toastmessage(
                    "Un email vous a été envoyé pour reinitialiser votre mot de passe");
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => const AuthenticateScreen()));
              }).onError((error, stackTrace) {
                toastmessage(error.toString());
              });
            }),
      ]),
    );
  }
}
