import 'package:Motivie/pages/coaching.dart';
import 'package:Motivie/pages/entrainement.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_chat_ui/flutter_chat_ui.dart';
import 'package:go_router/go_router.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'navigationDrawer/navigationDrawer.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_auth/firebase_auth.dart';

import 'pages/sante.dart';

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  static const String routeName = '/homePage';

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final PageController _pageViewController = PageController(initialPage: 0);
  int _activePage = 0;
  final List<Widget> _Pages = [
    const Coaching(),
    const Entrainement(),
    const Sante()
  ];
  @override
  void dispose() {
    super.dispose();
    _pageViewController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 3,
        child: Scaffold(
          backgroundColor: Colors.black,
          appBar: AppBar(
            title: const Text('Moti\'vie'),
          ),
          drawer: const navigationDrawer(),
          body: Stack(
            children: [
              PageView.builder(
                  controller: _pageViewController,
                  onPageChanged: (int index) {
                    setState(() {
                      _activePage = index;
                    });
                  },
                  itemCount: _Pages.length,
                  itemBuilder: (BuildContext context, int index) {
                    return _Pages[index];
                  }),
              Positioned(
                bottom: 0,
                left: 0,
                right: 0,
                height: 40,
                child: Container(
                  color: Colors.white30,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: List<Widget>.generate(
                        _Pages.length,
                        (index) => Padding(
                              padding:
                                  const EdgeInsets.symmetric(horizontal: 8),
                              child: InkWell(
                                onTap: () {
                                  _pageViewController.animateToPage(index,
                                      duration:
                                          const Duration(milliseconds: 300),
                                      curve: Curves.easeIn);
                                },
                                child: CircleAvatar(
                                  radius: 5,
                                  backgroundColor: _activePage == index
                                      ? Colors.greenAccent
                                      : Colors.black,
                                ),
                              ),
                            )),
                  ),
                ),
              ),
            ],
          ),
        ));
  }
}
