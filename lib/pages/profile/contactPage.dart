import 'package:flutter/material.dart';

import '../../navigationDrawer/navigationDrawer.dart';

// ignore: camel_case_types
class contactPage extends StatelessWidget {
  static const String routeName = '/contactPage';

  const contactPage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Contacts"),
        ),
        drawer: const navigationDrawer(),
        body: const Center(child: Text("Affichage des contacts")));
  }
}
